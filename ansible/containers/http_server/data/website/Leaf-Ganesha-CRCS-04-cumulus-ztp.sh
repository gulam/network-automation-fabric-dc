#!/bin/bash

# CUMULUS-AUTOPROVISIONING
sleep 5

net add hostname Leaf-Ganesha-CRCS-04  
net add bgp autonomous-system 65014
net add bgp neighbor evpn peer-group
net add bgp neighbor evpn remote-as external
net add bgp neighbor evpn ebgp-multihop 3
net add bgp neighbor evpn update-source lo

net add interface eth0 ip address 192.168.141.16/24
net add loopback lo ip address 10.0.0.6/32
net add vlan 13 alias Customer 1
net add vlan 13 ip address 192.168.13.3/24 
net add vlan 13 ip address-virtual 44:39:39:ff:00:13 192.168.13.1/24
net add vlan 13 mtu 9160
net add vlan 13 vlan-id 13 
net add vlan 13 vlan-raw-device bridge
net add vlan 13 vrf evpn-default 
net add vlan 43 alias kucing
net add vlan 43 ip address 192.168.43.3/24 
net add vlan 43 ip address-virtual 44:39:39:ff:00:13 192.168.43.1/24
net add vlan 43 mtu 9160
net add vlan 43 vlan-id 43 
net add vlan 43 vlan-raw-device bridge
net add vlan 43 vrf evpn-default 

net add interface swp3 bridge trunk vlans 13
net add vxlan vni13 vxlan id 13 
net add vxlan vni13 bridge access 13 
net add vxlan vni13 vxlan local-tunnelip 10.0.0.6 

net add interface swp3 bridge trunk vlans 43
net add vxlan vni43 vxlan id 43 
net add vxlan vni43 bridge access 43 
net add vxlan vni43 vxlan local-tunnelip 10.0.0.6 


net add routing route-map LOOPBACKS permit 10 match interface lo
net add bgp neighbor DC peer-group
net add bgp neighbor DC remote-as external
net add bgp ipv4 unicast redistribute connected route-map LOOPBACKS
net add bgp l2vpn evpn neighbor DC activate
net add bgp l2vpn evpn neighbor evpn activate
net add bgp l2vpn evpn advertise-all-vni
net add bgp l2vpn evpn advertise-svi-ip
net add bgp neighbor swp1 interface peer-group DC
net add bgp neighbor swp2 interface peer-group DC

net add vlan 500 vlan-id 500
net add vlan 500 alias L3VPN vlan
net add vlan 500 vrf evpn-default 
net add vxlan vni10500 vxlan id 10500 
net add vxlan vni10500 bridge access 500 
net add vxlan vni10500 vxlan local-tunnelip 10.0.0.6  
net add bridge bridge ports vni10500
net add vrf evpn-default vni 10500

net commit
sleep 5

reboot

# CUMULUS-AUTOPROVISIONING

exit 0


