#!/bin/bash

# CUMULUS-AUTOPROVISIONING
sleep 5

net add hostname Spine-Ganesha-CRCS-01  
net add bgp autonomous-system 65001
net add bgp neighbor evpn peer-group
net add bgp neighbor evpn remote-as external
net add bgp neighbor evpn ebgp-multihop 3
net add bgp neighbor evpn update-source lo

net add interface eth0 ip address 192.168.141.11/24
net add loopback lo ip address 10.0.0.1/32
net add interface swp2 ip address 169.254.0.0/31
net add interface swp3 ip address 169.254.0.2/31
net add interface swp7 ip address 169.254.0.8/31


net add routing route-map LOOPBACKS permit 10 match interface lo
net add bgp neighbor DC peer-group
net add bgp neighbor DC remote-as external
net add bgp ipv4 unicast redistribute connected route-map LOOPBACKS
net add bgp l2vpn evpn neighbor DC activate
net add bgp l2vpn evpn neighbor evpn activate
net add bgp l2vpn evpn advertise-all-vni
net add bgp l2vpn evpn advertise-svi-ip
net add bgp neighbor swp1 interface peer-group DC
net add bgp neighbor 10.0.0.4 peer-group evpn 
net add bgp neighbor swp2 interface peer-group DC
net add bgp neighbor 10.0.0.5 peer-group evpn 
net add bgp neighbor swp3 interface peer-group DC
net add bgp neighbor swp4 interface peer-group DC
net add bgp neighbor swp5 interface peer-group DC
net add bgp neighbor swp6 interface peer-group DC
net add bgp neighbor 10.0.0.10 peer-group evpn 
net add bgp neighbor swp7 interface peer-group DC


net commit
sleep 5

reboot

# CUMULUS-AUTOPROVISIONING

exit 0


